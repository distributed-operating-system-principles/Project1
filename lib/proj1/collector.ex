defmodule Proj1.Collector do
  use GenServer

  def start_link(opts \\ []) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def init(:ok) do
    {:ok, []}
  end

  def handle_cast({:start_listen, cnt, process}, state) do
    state = handle_listen({cnt, state})
    state = Enum.sort(state)
    send(process, {:fin, state})
    {:noreply, state}
  end

  defp handle_listen({0, state}) do
    state
  end

  defp handle_listen({cnt, state}) do
    {cnt, state} =
    receive do
      {:result, res} -> {cnt - 1, state ++ res}
    end
    handle_listen({cnt, state})
  end

end
