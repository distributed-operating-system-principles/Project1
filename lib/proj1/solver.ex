defmodule Proj1.Solver do
    use GenServer

    #API methods

    def start_link(opts \\ []) do
      GenServer.start_link(__MODULE__, :ok, opts)
    end

    def start_computation(process, {n, p, k}, from_process) do
      GenServer.cast(process, {:start_computation, {n, p, k}, from_process})
    end

    #Callbacks
    def init(:ok) do
      {:ok, []}
    end

    def handle_cast({:start_computation, {n, p, k}, process}, state) do
      state =
        if(rem(n, p) === 0) do
          compute_till_n({n, n - p + 1, k}, state)
        else
          compute_till_n({n, n - rem(n, p) + 1, k}, state)
        end
      send(process, {:result, state})
      {:noreply, state}
    end

    #Private functions

    defp compute_till_n({u, l, _k}, state) when u == l - 1 do
      state
    end

    defp compute_till_n({u, l, k}, state) do
      c = compute({u, k})
      state = check_if_perfectsquare({c, u}, state)
      compute_till_n({u - 1, l, k}, state)
    end

    defp squareroot(true, _xi) do
      true
    end
    defp squareroot(false, _xi) do
      false
    end
    defp squareroot(c, xi) do
      x = div(xi * xi + c, 2 * xi)
      c =
      if(x * x === c) do
        true
      else
        c
      end
      c =
      if((x * x > c && (x - 1) * (x - 1) < c) || (x * x < c && (x + 1) * (x + 1) > c)) do
        false
      else
        c
      end
      squareroot(c, x)
    end

    defp check_if_perfectsquare({c, u}, state) do
      state =
      if squareroot(c,:math.sqrt(c) |> :math.floor() |> trunc()) === true do
        #IO.puts("#{u}")
        [u | state]
      else
        state
      end
      state
    end

    defp compute({a, k}) do
      k * a * a + k * (k - 1) * a + div(k * (k - 1) * (2 * k - 1), 6)
    end

end
