defmodule Proj1.Aggregator do
    use DynamicSupervisor

    def start_link(opts \\ []) do
      DynamicSupervisor.start_link(__MODULE__, :ok, opts)
    end

    def start_children({_supervisor_process, _listening_process}, {0, _p, _k}, stat) do
      stat
    end

    def start_children({supervisor_process, listening_process}, {n, p, k}, stat) do
      {:ok, solver} = DynamicSupervisor.start_child(supervisor_process, Proj1.Solver)
      stat = [solver | stat]
      Proj1.Solver.start_computation(solver,{n, p, k}, listening_process)
      if(rem(n, p) === 0) do
        start_children({supervisor_process, listening_process}, {n - p, p, k}, stat)
      else
        start_children({supervisor_process, listening_process}, {n - rem(n, p), p, k}, stat)
      end
    end

    def init(:ok) do
      DynamicSupervisor.init(strategy: :one_for_one)
    end

end
