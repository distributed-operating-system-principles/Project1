defmodule Proj1.CLI do
    def run() do
      {_, args, _} = OptionParser.parse(System.argv(), strict: [])
      {n, _} = Enum.at(args, 0) |> Integer.parse()
      {k, _} = Enum.at(args, 1) |> Integer.parse()

      Process.flag(:trap_exit, true)

      {:ok, supervisor_process} = Proj1.Aggregator.start_link(name: Proj1.AggSupervisor)
      {:ok, listening_process} = Proj1.Collector.start_link()

      p = 5000
      cnt = :math.ceil(n / p) |> Kernel.trunc()

      GenServer.cast(listening_process, {:start_listen, cnt, self()})
      _list = Proj1.Aggregator.start_children({supervisor_process, listening_process}, {n, p, k},[])

      res =
        receive do
          {:fin, state} -> state
        end

      Process.exit(supervisor_process, :kill)
      Process.exit(listening_process, :kill)

      for i <- res do
        IO.puts(i)
      end

    end

end

Proj1.CLI.run()
