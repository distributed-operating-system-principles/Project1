# Description

An interesting problem in arithmetic with deep implications to elliptic curve theory is the problem of finding perfect squares that are sums of consecutive squares. A classic example is the Pythagorean identity:

3<sup>2</sup> + 4<sup>2</sup> = 5<sup>2</sup> (1) 

that reveals that the sum of squares of 3, 4 is itself a square. 
A more interesting example is Lucas‘ Square Pyramid:

1<sup>2</sup> +2<sup>2</sup> +...+24<sup>2</sup> =70<sup>2</sup> (2)

In both of these examples, sums of squares of consecutive integers form the square of another integer.
The goal of this first project is to use Elixir and the actor model to build a good solution to this problem that runs well on multi-core machines.

**Instructions**

1. Open Project1 directory
2. Open terminal within this directory
3. Run "mix compile"
4. Run "mix run proj1.exs <N> <k>"

# Project Info

**Work Unit**

Size of the work unit = 5000

We are using the Actor model by using GenServer for the child processes and DynamicSupervisor for the boss process.

Our application adapts the number of processes with the value of the input N. As N increases, the number of processes increases. We decided 
the work unit for each process to be 5000, because it is a considerable amount of computation. We tried to verify if this work unit yields good 
results.

We varied the work unit from 5 to 1000000 and calculated the real time taken and the effective cores utilized. We got the best results for work 
units around 5000.
Time statistics for different work unit lengths are provided at the end of this section.

**Sample Solution**

Result for input "mix run proj1.exs 1000000 4" is empty list.

Time stats for the above input is:
real	0m0.462s
user	0m1.137s
sys	0m0.067s

Effective cores utilized = 2.6

**Largest Problem Solved**

The largest input we solved is 1000000000 and the time stats for this input is:

Input: "time mix run proj1.exs 1000000000 24"

real	5m31.668s

user	21m56.050s

sys	0m1.162s

Effective cores utilized = 3.97

# Time Statistics 
```
time mix run proj1.exs 1000000 24
```

**Work Unit = 5**

real	0m3.322s

user	0m6.242s

sys	0m0.535s

Effective-Cores = 2.04

----------------------

**Work Unit = 100**

real	0m0.592s

user	0m1.055s

sys	0m0.064s

Effective-Cores = 1.89

----------------------

**Work Unit = 500**

real	0m0.511s

user	0m0.890s

sys	0m0.057s

Effective-Cores = 1.85

----------------------

**Work Unit = 1000**

real	0m0.514s

user	0m0.852s

sys	0m0.071s

Effective-Cores = 1.79

----------------------

**Work Unit = 5000**

real	0m0.477s

user	0m1.164s

sys	0m0.050s

Effective-Cores = 2.54

----------------------

**Work Unit = 10000**

real	0m0.500s

user	0m1.145s

sys	0m0.078s

Effective-Cores = 2.45

----------------------

**Work Unit = 50000**

real	0m0.481s

user	0m1.124s

sys	0m0.039s

Effective-Cores = 2.42

----------------------

**Work Unit = 100000**

real	0m0.485s

user	0m1.083s

sys	0m0.068s

Effective-Cores = 2.37

----------------------

**Work Unit = 1000000**

real	0m0.638s

user	0m0.814s

sys	0m0.054s

Effective-Cores = 1.36

--------------------------